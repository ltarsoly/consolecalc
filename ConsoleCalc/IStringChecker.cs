namespace ConsoleCalc
{
    public interface IStringChecker
    {
        int? Check(string str);
    }
}