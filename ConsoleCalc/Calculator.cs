using System;

namespace ConsoleCalc
{
    public class Calculator
    {
        private readonly IStringChecker checker;

        public Calculator(IStringChecker checker)
        {
            this.checker = checker;
        }

        public int Evaluate(string exp)
        {
            var initValue = checker.Check(exp);

            if (initValue.HasValue)
            {
                return initValue.Value;
            }

            var operand1 = int.Parse(exp.Substring(0, exp.IndexOf("+", StringComparison.Ordinal)));
            var operand2 = int.Parse(exp.Substring(exp.IndexOf("+", StringComparison.Ordinal) + 1));

            return operand1 + operand2;
        }
    }
}