﻿using System;

namespace ConsoleCalc
{
    class Program
    {
        static void Main(string[] args)
        {
            //use a DI framework here to instantiate calculator, and inject an instance of StringChecker to the calculator
            var calc = new Calculator(new StringChecker());
            Console.WriteLine("Result: {0}", calc.Evaluate(args[0]));
            //XX
        }
    }
}
