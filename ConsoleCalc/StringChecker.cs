using System;

namespace ConsoleCalc
{
    public class StringChecker : IStringChecker
    {
        public int? Check(string str)
        {
            if (str == null)
            {
                throw new ArgumentNullException();
            }

            if (str.Length == 0)
            {
                return 0;
            }

            return null;
        }
    }
}