﻿using System;
using ConsoleCalc;
using NUnit.Framework;

namespace Tests
{
    [TestFixture]
    public class StringCheckerTests
    {
        [Test]
        public void CheckerShouldReturnZeroForEmptyString()
        {
            var str = string.Empty;

            StringChecker checker = new StringChecker();

            var result = checker.Check(str);

            Assert.That(result, Is.EqualTo(0));
        }
        
        [Test]
        public void CheckerShouldThrowExceptionOnNull()
        {
            string str = null;

            StringChecker checker = new StringChecker();

            int result;

            Assert.Throws<ArgumentNullException>(()=>checker.Check(str));
        }

        [Test]
        public void CheckerShouldreturnNullIfStringIsAsExpected()
        {
            string str = " ";

            StringChecker checker = new StringChecker();

            int? result = checker.Check(str);

            Assert.That(result, Is.Null);
        }
    }
}
