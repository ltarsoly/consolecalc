﻿using System;
using ConsoleCalc;
using NUnit.Framework;

namespace Tests
{
    [TestFixture]
    public class CalculatorTests
    {
        [Test]
        public void CalculatorShouldReturnTenForFivePlusFive()
        {
            //given
            var exp = "5+5";

            var stubchecker = new StubChecker(null);

            var calculator = new Calculator(stubchecker);

            //when
            int result = calculator.Evaluate(exp);

            //then
            Assert.That(result, Is.EqualTo(10));
        }

        [Test]
        public void CalculatorShouldReturnElevenForFivePlusSix()
        {
            //given
            var exp = "5+6";
            var stubchecker = new StubChecker(null);

            var calculator = new Calculator(stubchecker);

            //when
            int result = calculator.Evaluate(exp);

            //then
            Assert.That(result, Is.EqualTo(11));
        }

        [Test]
        public void CalculatorShouldReturnZeroForEmptyString()
        {
            //given
            var exp = string.Empty;

            var stubchecker = new StubChecker(0);

            var calculator = new Calculator(stubchecker);

            //when
            int result = calculator.Evaluate(exp);

            //then
            Assert.That(result, Is.EqualTo(0));
        }

        [Test]
        public void CalculatorShouldThrowArgumentNullExceptionForNullExpressionString()
        {
            //given
            string exp = null;

            var checker = new StubCheckerThrowsException();

            var calculator = new Calculator(checker);

            //when
            int result;

            //then
            Assert.Throws<ArgumentNullException>(() => { calculator.Evaluate(exp); });
        }

    }
}
