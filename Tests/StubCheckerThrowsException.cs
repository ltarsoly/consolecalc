﻿using System;
using ConsoleCalc;

namespace Tests
{
    public class StubCheckerThrowsException : IStringChecker
    {
        public int? Check(string str)
        {
            throw new ArgumentNullException();
        }
    }
}