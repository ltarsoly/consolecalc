﻿using ConsoleCalc;

namespace Tests
{
    public class StubChecker : IStringChecker
    {
        private readonly int? retval;

        public StubChecker(int? retval)
        {
            this.retval = retval;
        }

        public int? Check(string str)
        {
            return retval;
        }
    }
}